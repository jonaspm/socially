import angular from 'angular';
import angularMeteor from 'angular-meteor';
import { Parties } from '../../../api/parties';
import PartyAdd from '../partyAdd/partyAdd';
import PartyRemove from '../partyRemove/partyRemove';

import templateUrl from './partiesList.html';

class PartiesList {
	constructor($scope) {
		$scope.viewModel(this);

		this.helpers({
			parties() {
				return Parties.find({});
			}
		});
  	}
}

const name = 'partiesList';

// create a module
export default angular.module(name, [
	angularMeteor,
	PartyAdd.name,
	PartyRemove.name
]).component(name, {
	templateUrl,
	controllerAs: name,
	controller: ['$scope', PartiesList]
});

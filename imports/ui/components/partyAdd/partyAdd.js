import angular from 'angular';
import angularMeteor from 'angular-meteor';
import { Parties } from '../../../api/parties';

import templateUrl from './partyAdd.html';

class PartyAdd {
	constructor() {
		this.party = {};
	}

	submit() {
		Parties.insert(this.party);
		this.reset();
	}

	reset() {
		this.party = {};
	}
}

const name = 'partyAdd';

export default angular.module(name, [
	angularMeteor
]).component(name, {
	templateUrl,
	controllerAs: name,
	controller: PartyAdd
});

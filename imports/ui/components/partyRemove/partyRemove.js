import angular from 'angular';
import angularMeteor from 'angular-meteor';
import { Parties } from '../../../api/parties';

import templateUrl from './partyRemove.html';

class PartyRemove {
	remove() {
		if (this.party) {
			Parties.remove(this.party._id);
		}
	}
}

const name = 'partyRemove';

export default angular.module(name, [
	angularMeteor
]).component(name, {
	templateUrl,
	bindings: {
		party: '<'
	},
	controllerAs: name,
	controller: PartyRemove
});

import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router'

import PartiesList from '../partiesList/partiesList';
import templateUrl from './socially.html';

class Socially {}

const name = 'socially';

export default angular.module(name, [
	angularMeteor,
	uiRouter,
	PartiesList.name
])
.component(name, {
	templateUrl,
	controllerAs: name,
	controller: Socially
})
.config(config);

function config($locationProvider, $urlRouterProvider) {

	$locationProvider.html5Mode(true);

	$urlRouterProvider.otherwise('/parties');
}
